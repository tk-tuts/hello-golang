package main

import (
	"net/http"
	"fmt"
)

func main() {
	startServer()
}

func startServer() {
	fmt.Println("Starting server")
	http.HandleFunc("/", handleRoot)

	http.HandleFunc("/post", handlePost)

	err := http.ListenAndServe(":8080", nil)
	fmt.Println("Server is running")
	if err != nil {
		panic(err);
	}

	defer fmt.Println("server is stopped")
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hello")
}

func handlePost(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("use POST"))
		return
	}

	name := r.FormValue("name")
	fmt.Fprint(w, name)
}
