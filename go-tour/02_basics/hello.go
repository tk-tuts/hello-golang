package main

import (
	"fmt"
)

// type at the end
var c, python, java bool

// initialization
var a, b int = 1, 2

// type omitted
var d, e = "d", "e"

func main() {
	fmt.Println("\n Variables")
	fmt.Println("-----------------")

	var i int
	fmt.Println(i, c, python, java)

	fmt.Println(a, b)
	fmt.Printf("%s %s\n", d, e);

	// short variable declaration (without var)
	f := 4
	fmt.Println(f)

	// numeric conversion
	var g int = 42
	fmt.Println(uint(g))

	// type inference
	var h int = 43
	j := h
	fmt.Println(j)

	// CONSTANTS
	const Pi = 3.14
	fmt.Println("Pi", Pi)
}
